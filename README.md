# ABP-Chromium-test-automation

This repo contains the test automation framework and an example of a test that run against ABP Chromium using [appium](http://appium.io/), java and [cucumber](https://cucumber.io/).

We use the following website (https://testpages.adblockplus.org/) that contains test pages for different filter options.

Unlike the desktop version where it's possible to add one filter at a time, for android, before you start testing you need to subscribe to the test filter list (https://testpages.adblockplus.org/en/abp-testcase-subscription.txt
) to have all necessary filters.

## Pre-requisites

Our version of the [appium docker image](https://gitlab.com/eyeo/docker/-/tree/master/appium/1.16.0) should be running locally.
If you don't own an android device, you may also set up and run appium server locally and use an emulator.
Both arm and x86 builds are provided.

## Running the tests

There are 2 options of running the tests.

To automatically install the APK and run the tests the `start-tests.sh` script can be used. It takes either a path or URL to the APK to be tested.
For this Case Study, the APKs for ABP Chromium 85v1 is located in src/test/resources/APKs

Example:
```
./start-tests.sh src/test/resources/APKs/ABPChromium-84-arm.apk

```

You may also install the APK on the device before running the test and then run the gradle task directly, this will provide a more readable output and make it easier to debug failures in the tests.
To do this you must also set the `CHROME_VERSION` environment variable so gradle knows which version of the chromedriver to use.

```
CHROME_VERSION=84 ./gradlew cucumber
```
