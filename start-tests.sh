#!/bin/bash
# This small script should be used to launch tests.
# You can pass either a path or URL to an APK

# Name of the container running your appium server (also read from $APPIUM_CONTAINER)
CONTAINER_NAME=${APPIUM_CONTAINER:-appium}

if [ -z "$1" ] ; then
    echo "Please supply path or URL to APK to test"
    exit 1
fi

APK="$1"
set -e

if ! which aapt >/dev/null ; then
    echo "aapt is required and should be in your PATH"
    exit 1
fi


function cleanup {
    if [ "${DOWNLOADED}" == 'yes' ] ; then
        rm -f $APK &>/dev/null || true
    fi
}
trap cleanup EXIT

if echo $APK | grep -q "^http" ; then
    URL=$APK
    echo "Downloading APK from URL: ${URL}"
    APK=$(mktemp -t tmp-downloaded-XXXXXX.apk)
    wget --no-verbose -O $APK $URL
    DOWNLOADED=yes
fi

if [ ! -f $APK ] ; then
    echo "Unable to read APK \"$APK\""
    exit 1
fi

# only works on GNU grep. FU mac os.
# export CHROME_VERSION=$(aapt dump badging $APK | grep -Po "(?<=\sversionName=')([0-9]+)")
export CHROME_VERSION=$(aapt dump badging $APK | grep versionName | grep -Eo "(versionName=')([0-9]+)" | sed 's/[^0-9]*//g')
if [ -z "$CHROME_VERSION" ] ; then
    exit 1
fi
echo "Detected chrome version ${CHROME_VERSION}"

if docker ps --format '{{.Names}}' | grep -q ${CONTAINER_NAME} ; then
    echo "Copying APK to appium container..."
    LOCAL_APK=$APK
    APK=/tmp/$(basename $APK)
    docker cp $LOCAL_APK ${CONTAINER_NAME}:${APK}
    cleanup
else
    if [ "${DOWNLOADED}" == 'yes' ] ; then
        echo "Unable to find running container named ${CONTAINER_NAME}"
        echo "Your APK will be downloaded again by Appium"
        APK=${URL}
    else
        echo "Error: cannot copy local file to remote appium server"
        echo "Try specifing your APK via a URL"
        exit 1
    fi
fi


echo "Setting environment variable CHROME_VERSION to \"$CHROME_VERSION\" and APK to \"$APK\""

CHROME_VERSION="$CHROME_VERSION" APK="$APK" ./gradlew test
