package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilterOptionsPage extends BasePage {
    public FilterOptionsPage(AndroidDriver driver) {
        super(driver);
    }

    By blockedElements = By.cssSelector("[data-expectedresult='fail']");

    // Requirement not clear so I am checking elements are visible or not
    // All elements in the out-of the frame and in the iframe
    By elementsOutIFrame = By.cssSelector(".testcase-area");
    By elementsIframe = By.cssSelector("body");
    By addiFrame = By.cssSelector("iframe[src='/sitekey-frame']");

    public int getNumberOfBlockedImages() {
        List<WebElement> elements = findElementsWaitPresence(blockedElements);
        List<String> logs = getBrowserLogs();
        int numberOfBlockedImgs = 0;
        for (WebElement element : elements) {
            String imgUrl = element.getAttribute("src");
            for (String line : logs) {
                if (line.contains("Failed to load") && line.contains(imgUrl)) {
                    numberOfBlockedImgs++;
                }
            }
        }
        return numberOfBlockedImgs;
    }

    private int getNumberOfHiddenElements(By locator) {
        List<WebElement> elements = findElementsWaitPresence(locator);
        int numberOfHiddenImgs = 0;
        for (WebElement element : elements) {
            if(!element.isDisplayed()){
                numberOfHiddenImgs++;
            }
        }
        return numberOfHiddenImgs;
    }

    public int getNumberOfHiddenImages(){
        return getNumberOfHiddenElements(blockedElements);
    }

    public Map<String, Integer> getNumberOfBlockedElements(Boolean iframe) {
        Map<String, Integer> elementsMap = new HashMap<>();
        List<WebElement> elements;
        if (!iframe) {
            elements = findElementsWaitPresence(elementsOutIFrame);
        } else {
            WebElement iFrame = driver.findElement(addiFrame);
            elements = findElementsWaitPresenceIframe(elementsIframe, iFrame);
        }
        int numberOfBlockedElements = 0;
        int numberOfNonBlockedElements = 0;
        for (WebElement element : elements) {
            if(element.isDisplayed()){
                numberOfNonBlockedElements++;
            } else {
                numberOfBlockedElements++;
            }
        }
        elementsMap.put("blocked", numberOfBlockedElements);
        elementsMap.put("nonblocked", numberOfNonBlockedElements);
        return elementsMap;
    }


}
