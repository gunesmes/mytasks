package pages;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;

import java.time.Duration;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class GenericPage extends BasePage {

    public GenericPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = "org.chromium.chrome:id/home_button")
    private AndroidElement homeBtn;

    @AndroidFindBy(id = "org.chromium.chrome:id/search_provider_logo")
    private AndroidElement googleLogo;

    @AndroidFindBy(id = "org.chromium.chrome:id/menu_button")
    private AndroidElement menuBtn;

    @AndroidFindBy(accessibility = "Settings")
    private AndroidElement settingsBtn;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[5]/android.widget.RelativeLayout")
    private AndroidElement adBlockingBtn;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[1]")
    private AndroidElement abp;

    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.LinearLayout\").index(1)")
    private AndroidElement abpSwitchSection;

    @AndroidFindBy(id = "org.chromium.chrome:id/fragment_adblock_custom_item_add_button")
    private AndroidElement plusBtn;

    @AndroidFindBy(id = "org.chromium.chrome:id/fragment_adblock_custom_item_add_label")
    private AndroidElement textBox;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.RelativeLayout")
    private AndroidElement addList;

    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.TextView\").text(\"Filter lists\")")
    public AndroidElement filterLists;

    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.TextView\").text(\"Acceptable Ads\")")
    public AndroidElement acceptableAds;


    public String goToContext(String context) {
        changeContext(context);
        String currentContext = driver.getContext();
        return currentContext;
    }


    public void iAmHomePage() {
        changeContext("NATIVE");
        homeBtn.click();
        isElementDisplayed(googleLogo);
    }

    public void goToAdBlockSettings() {
        changeContext("NATIVE");
        menuBtn.click();
        isElementDisplayed(settingsBtn);
        settingsBtn.click();
        scrollAndClick("Ad blocking");
    }

    public void enableMoreOptions() {
        TouchAction touchAction=new TouchAction(driver);
        int x = abpSwitchSection.getCenter().x;
        int y = abpSwitchSection.getCenter().y;
        for (int taps = 0; taps < 12; taps++) {
            touchAction.tap(PointOption.point(x, y)).waitAction(waitOptions(Duration.ofMillis(50))).perform();
        }
    }


    public void sendTextToElement(String text) {
        textBox.sendKeys(text);
    }

    public void clickPlusBtn() {
        plusBtn.click();
    }

    public void clickFilter() {
        addList.click();
    }

    By filtersPageTitle = By.cssSelector("section.site-panel > h2");

    public String getFiltersPageTitle(){
        String title = findElementWaitVisibility(filtersPageTitle).getText();
        return title;
    }

    public void clickAcceptableAds() { acceptableAds.click(); }
}
