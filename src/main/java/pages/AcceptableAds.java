package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AcceptableAds extends BasePage {

    public AcceptableAds(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }
    By iFrame= By.cssSelector("iframe");
    By googleAddFrame = By.id("gencats");

    public boolean getVisibilityOfIFrame() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(googleAddFrame));
        try {
            return driver.findElement(iFrame).isDisplayed();
        } catch (WebDriverException e) {
            return false;
        }

    }
}
