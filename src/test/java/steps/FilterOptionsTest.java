package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.FilterOptionsPage;

import java.util.Map;

public class FilterOptionsTest {
    protected BaseTest baseTest = new BaseTest();
    protected FilterOptionsPage filterOptionsPage = new FilterOptionsPage(baseTest.getDriver());


    @Then("I verify {int} image(s) (were)(was) blocked")
    public void verifyImagesBlocked(int expected){
        int actual = filterOptionsPage.getNumberOfBlockedImages();
        Assert.assertEquals("Test failed. Number of blocked images are not correct.",expected,actual);
    }

    @Then("I verify {int} image(s) (were)(was) hidden")
    public void verifyImagesHidden(int expected){
        int actual = filterOptionsPage.getNumberOfHiddenImages();
        Assert.assertEquals("Test failed. Number of hidden images are not correct.",expected,actual);
    }

    @And("I verify all elements were blocked")
    public void verifyAllElementBlocked() {
        int actual = filterOptionsPage.getNumberOfBlockedImages();
        Assert.assertEquals("Test failed. ALl images are not blocked", 0, actual);
    }

    @And("I verify all elements out-of-the-frame were blocked")
    public void verifyAllElementsOutOfTheFrameWereBlocked() {
        Map<String, Integer> actual = filterOptionsPage.getNumberOfBlockedElements(false);
        int nonblocked = actual.get("nonblocked");
        Assert.assertEquals("Test failed. ALl elements out-of-the-frame are not blocked", 2, nonblocked);
    }

    @And("I verify all elements in-the-frame were unblocked")
    public void verifyAllElementsInTheFrameWereUnblocked() {
        Map<String, Integer> actual = filterOptionsPage.getNumberOfBlockedElements(true);
        int blocked = actual.get("blocked");
        Assert.assertEquals("Test failed. ALl elements are not unblocked", 0, blocked);
    }

    @And("I verify all elements in-the-frame were blocked")
    public void verifyAllElementsInTheFrameWereBlocked() {
        Map<String, Integer> actual = filterOptionsPage.getNumberOfBlockedElements(true);
        int unblocked = actual.get("nonblocked");
        Assert.assertEquals("Test failed. ALl elements in-the-frame are not blocked", 0, unblocked);
    }
}
