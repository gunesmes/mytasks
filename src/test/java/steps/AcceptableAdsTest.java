package steps;

import io.cucumber.java.en.And;
import org.junit.Assert;
import pages.AcceptableAds;

public class AcceptableAdsTest {
    protected BaseTest baseTest = new BaseTest();
    protected AcceptableAds acceptableAds = new AcceptableAds(baseTest.getDriver());
    
    @And("I verify ads are visible")
    public void verifyAdsAreVisible() {
        boolean actual = acceptableAds.getVisibilityOfIFrame();
        Assert.assertEquals("Test failed. Number of hidden images are not correct.",true, actual);
    }

    @And("I verify ads are not visible")
    public void verifyAdsAreNotVisible() {
        boolean actual = acceptableAds.getVisibilityOfIFrame();
        Assert.assertEquals("Test failed. Number of hidden images are not correct.",false, actual);
    }
}
