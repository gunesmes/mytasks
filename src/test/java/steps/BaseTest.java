package steps;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

public class BaseTest {
    private static AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel 3");
        capabilities.setCapability("clearDeviceLogsOnStart", true);
        ChromeOptions chrome_options = new ChromeOptions();
        chrome_options.setExperimentalOption("androidPackage", "org.chromium.chrome");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chrome_options);
        capabilities.setCapability("otherApps",System.getenv("APK"));
        capabilities.setCapability("chromedriverExecutable","/chromedriver/"+System.getenv("CHROME_VERSION"));
        //capabilities.setCapability("chromedriverExecutable","/usr/local/bin/chromedriver");

        capabilities.setCapability("androidInstallTimeout", 180000 );

        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), capabilities);

    }

    public AndroidDriver<AndroidElement> getDriver() {
        return driver;
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
