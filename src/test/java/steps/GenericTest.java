package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.GenericPage;

import java.util.Arrays;
import java.util.List;


public class GenericTest {
    protected BaseTest baseTest = new BaseTest();
    protected GenericPage genericPage = new GenericPage(baseTest.getDriver());

    @Given("I open ABP Chromium and Google Homepage is displayed")
    public void iAmHomePage() {
        genericPage.iAmHomePage();
    }

    @And("I go to AdBlocking Settings")
    public void goToAdBlockSettings() {
        genericPage.goToAdBlockSettings();
    }

    @Then("I add {string} filter list URL")
    public void addFilterList(String url) {
        genericPage.goToAdBlockSettings();
        genericPage.enableMoreOptions();
        genericPage.scrollAndClick("More blocking options");
        genericPage.clickFilter();
        genericPage.sendTextToElement(url);
        genericPage.clickPlusBtn();
    }

    @And("I am at {string} test page")
    public void goToTestPages(String extention){
        List<String> arr = Arrays.asList(extention.split("/"));
        String filter = arr.get(arr.size()-1);
        genericPage.goToContext("WEBVIEW");
        genericPage.goToUrl("https://testpages.adblockplus.org/"+extention);
        String title = genericPage.getFiltersPageTitle().toLowerCase();
        Assert.assertEquals(filter, title);
    }

    @And("I go back to the Main Page")
    public void goBackToMainPage() throws InterruptedException {
        genericPage.pressBackButton(4);
    }

    @And("^I add following filters$")
    public void addFilterTable(DataTable table) {

        genericPage.goToAdBlockSettings();
        genericPage.enableMoreOptions();
        genericPage.scrollAndClick("More blocking options");
        genericPage.clickFilter();

        List<List<String>> rows = table.asLists(String.class);
        for (List<String> columns : rows) {
            genericPage.sendTextToElement(columns.get(0));
            genericPage.clickPlusBtn();
        }
    }

    @When("I add more filters")
    public void addMoreFilters(DataTable table) {

        genericPage.goToAdBlockSettings();
        genericPage.scrollAndClick("More blocking options");
        genericPage.clickFilter();

        List<List<String>> rows = table.asLists(String.class);
        for (List<String> columns : rows) {
            genericPage.sendTextToElement(columns.get(0));
            genericPage.clickPlusBtn();
        }
    }

    @Then("I am at {string} page")
    public void iAmAtPage(String url) throws InterruptedException {
        genericPage.goToContext("WEBVIEW");
        genericPage.goToUrl(url);
    }

    @And("I set acceptable ads {string}")
    public void iSetAcceptableAds(String state) {
        genericPage.goToAdBlockSettings();
        genericPage.clickAcceptableAds();
    }
}
