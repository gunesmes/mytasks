Feature: Site Key Exception Filter

  Scenario: Verify Site Key Exception filters are working properly
    Given I open ABP Chromium and Google Homepage is displayed
    And I add following filters
      |https://testpages.adblockplus.org/testfiles/sitekey/outofframe.png|
      |https://testpages.adblockplus.org/testfiles/sitekey/inframe.png|
      |https://testpages.adblockplus.org/images/abp-32.png|
    Then I go back to the Main Page
    Then I am at 'en/exceptions/sitekey' test page
    And I verify all elements out-of-the-frame were blocked
    And I verify all elements in-the-frame were blocked
    When I add more filters
      |\@\@\$document,sitekey=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANGtTstne7e8MbmDHDiMFkGbcuBgXmiVesGOG3gtYeM1EkrzVhBjGUvKXYE4GLFwqty3v5MuWWbvItUWBTYoVVsCAwEAAQ|
    Then I go back to the Main Page
    Then I am at 'en/exceptions/sitekey' test page
    And I verify all elements out-of-the-frame were blocked
    And I verify all elements in-the-frame were unblocked
